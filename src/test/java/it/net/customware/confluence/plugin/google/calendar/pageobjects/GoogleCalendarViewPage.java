package it.net.customware.confluence.plugin.google.calendar.pageobjects;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.WebDriverElement;
import org.openqa.selenium.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.pageobjects.elements.query.Poller.by;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.core.Is.is;

public class GoogleCalendarViewPage extends ViewPage {
    private static final String CALENDAR_EVENTS_SELECTOR = "div.rb-ni";
    private static final String CALENDAR_EVENTS_WRAPPER_SELECTOR = "div.rb-n";
    private static final String CALENDAR_FRAME_SELECTOR = "iframe[data-macro-name=google-calendar]";
    private static final String CALENDAR_WEEK_DAYS_SELECTOR = "span.wk-daylink";

    private JavascriptExecutor javascriptExecutor;
    private WebDriver webDriver;

    @ElementBy(id = "calendarTitle")
    private PageElement calendarTitle;

    @ElementBy(id = "currentDate1")
    private PageElement currentDate;

    @ElementBy(cssSelector = CALENDAR_WEEK_DAYS_SELECTOR)
    private PageElement firstCalendarDay;

    @ElementBy(cssSelector = CALENDAR_EVENTS_SELECTOR)
    private PageElement firstCalendarEvent;

    @ElementBy(cssSelector = CALENDAR_EVENTS_WRAPPER_SELECTOR)
    private PageElement firstCalendarEventWrapper;

    @ElementBy(cssSelector = CALENDAR_FRAME_SELECTOR)
    private WebDriverElement googleCalendarFrame;

    @ElementBy(tagName = "body")
    private PageElement googleCalendarFrameBody;

    @ElementBy(id = "todayButton1")
    private PageElement todayButton;

    public GoogleCalendarViewPage(Content content, WebDriver webDriver) {
        super(content);
        this.webDriver = webDriver;
        this.javascriptExecutor = (JavascriptExecutor) webDriver;
    }

    private static String getFirstStyleProperty(WebElement element) {
        Optional<String> stringOptional = Optional.ofNullable(element.getAttribute("style"));
        return stringOptional.orElse(EMPTY).split(";")[0];
    }

    private static String getFirstStyleProperty(PageElement element) {
        Optional<String> stringOptional = Optional.ofNullable(element.getAttribute("style"));
        return stringOptional.orElse(EMPTY).split(";")[0];
    }

    public String getCurrentDate() {
        switchToGoogleCalendarFrame();
        return currentDate.getText();
    }

    public void setCurrentDate(String date) {
        switchToMainWindow();
        // In order to assert with the same events and dates even as time passes, a JS executor is used to add a query param
        // and force Google Calendar frame to load on the required date. Using the date picker UI is less consistent as
        // it would involve many clicks and change with time.
        javascriptExecutor.executeScript(
                "document.querySelector('" + CALENDAR_FRAME_SELECTOR + "').src += '&dates=" + date + "';");
    }

    public Dimension getFrameDimensions() {
        switchToMainWindow();
        return googleCalendarFrame.getSize();
    }

    public String getCalendarTitle() {
        switchToGoogleCalendarFrame();
        return calendarTitle.getText();
    }

    public String getFirstCalendarDay() {
        switchToGoogleCalendarFrame();
        return firstCalendarDay.getText();
    }

    public String getBodyBackgroundColor() {
        switchToGoogleCalendarFrame();
        return getFirstStyleProperty(googleCalendarFrameBody);
    }

    public String getTodayButtonText() {
        switchToGoogleCalendarFrame();
        return todayButton.getText();
    }

    public List<String> getCalendarDays() {
        switchToGoogleCalendarFrame();
        waitUntil(firstCalendarDay.timed().isPresent(), is(true), by(10, SECONDS));
        return webDriver.findElements(By.cssSelector(CALENDAR_WEEK_DAYS_SELECTOR)).stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public List<String> getCalendarEvents() {
        switchToGoogleCalendarFrame();
        waitUntil(firstCalendarEvent.timed().isPresent(), is(true), by(10, SECONDS));
        return webDriver.findElements(By.cssSelector(CALENDAR_EVENTS_SELECTOR)).stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public Set<String> getCalendarEventsColors() {
        switchToGoogleCalendarFrame();
        waitUntil(firstCalendarEventWrapper.timed().isPresent(), is(true), by(10, SECONDS));
        return webDriver.findElements(By.cssSelector(CALENDAR_EVENTS_WRAPPER_SELECTOR)).stream()
                .map(GoogleCalendarViewPage::getFirstStyleProperty)
                .collect(Collectors.toSet());
    }

    private void switchToGoogleCalendarFrame() {
        switchToMainWindow();
        waitUntil(googleCalendarFrame.timed().isPresent(), is(true), by(20, SECONDS));
        webDriver.switchTo().frame(googleCalendarFrame.asWebElement());
    }

    private void switchToMainWindow() {
        webDriver.switchTo().window("");
    }
}
