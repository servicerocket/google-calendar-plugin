package it.net.customware.confluence.plugin.google.calendar.stateless;

import com.atlassian.confluence.api.model.content.Content;
import com.google.common.base.Joiner;
import it.net.customware.confluence.plugin.google.calendar.AbstractGoogleCalendarTestRunner;
import it.net.customware.confluence.plugin.google.calendar.pageobjects.GoogleCalendarViewPage;
import org.junit.Test;
import org.openqa.selenium.Dimension;

import java.awt.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;

public class GoogleCalendarMacroTest extends AbstractGoogleCalendarTestRunner {

    @Test
    public void shouldBeAbleToDisplayMultipleCalendarsWithDifferentColors() {
        String targetMonth = "20200701/20200731";
        String formattedTargetMonth = "July 2020";
        String event1 = "Hari Raya Haji";
        String event2 = "Independence Day";
        String hexColor1 = "#A32929";
        String hexColor2 = "#0D7813";

        String wikiMarkup = createGoogleCalendarWikiWithParams(
                String.join(",", MALAYSIA_HOLIDAYS_CALENDAR, US_HOLIDAYS_CALENDAR),
                "colors=" + String.join(",", hexColor1, hexColor2));
        Content content = createPage(randomAlphanumeric(10), wikiMarkup);

        GoogleCalendarViewPage googleCalendarViewPage = visitGoogleCalendarViewPage(content);

        assertThat(googleCalendarViewPage.getCurrentDate(), equalTo(getFormattedCurrentMonthDate()));
        googleCalendarViewPage.setCurrentDate(targetMonth);
        assertThat(googleCalendarViewPage.getCurrentDate(), equalTo(formattedTargetMonth));
        assertThat(googleCalendarViewPage.getCalendarEvents(), hasItems(event1, event2));
        assertThat(googleCalendarViewPage.getCalendarEventsColors(), containsInAnyOrder(
                endsWith(convertHexToRgbColor(hexColor1)),
                endsWith(convertHexToRgbColor(hexColor2))));
    }

    @Test
    public void shouldBeAbleToSetDimensionsAndTitle() {
        String title = randomAlphanumeric(10);
        int width = nextInt(400, 500);
        int height = nextInt(750, 850);
        Dimension frameDimension = new Dimension(width, height);

        String wikiMarkup = createGoogleCalendarWikiWithParams(
                MALAYSIA_HOLIDAYS_CALENDAR,
                "width=" + width,
                "height=" + height,
                "title=" + title,
                "controls=all");
        Content content = createPage(randomAlphanumeric(10), wikiMarkup);

        GoogleCalendarViewPage googleCalendarViewPage = visitGoogleCalendarViewPage(content);

        assertThat(googleCalendarViewPage.getFrameDimensions(), equalTo(frameDimension));
        assertThat(googleCalendarViewPage.getCalendarTitle(), equalTo(title));
    }

    @Test
    public void shouldBeAbleToChangeModeAndFirstDayOfTheWeek() {
        String mode = "week";
        String day = DayOfWeek.values()[nextInt(0, 7)].name().toLowerCase();

        String wikiMarkup = createGoogleCalendarWikiWithParams(
                MALAYSIA_HOLIDAYS_CALENDAR,
                "mode=" + mode,
                "firstDay=" + day);
        Content content = createPage(randomAlphanumeric(10), wikiMarkup);

        GoogleCalendarViewPage googleCalendarViewPage = visitGoogleCalendarViewPage(content);

        List<String> calendarDays = googleCalendarViewPage.getCalendarDays();
        assertThat(calendarDays, hasSize(7));
        assertThat(calendarDays, everyItem(containsString("/")));
        assertThat(googleCalendarViewPage.getFirstCalendarDay().toLowerCase(), startsWith(day.substring(0, 3)));
    }

    @Test
    public void shouldBeAbleToChangeLanguageAndBackgroundColor() {
        String language = "fr";
        String todayInFrench = "Aujourd'hui";
        String backgroundColor = "#808080";

        String wikiMarkup = createGoogleCalendarWikiWithParams(
                MALAYSIA_HOLIDAYS_CALENDAR,
                "bgcolor=" + backgroundColor,
                "language=" + language);
        Content content = createPage(randomAlphanumeric(10), wikiMarkup);

        GoogleCalendarViewPage googleCalendarViewPage = visitGoogleCalendarViewPage(content);
        assertThat(googleCalendarViewPage.getTodayButtonText(), equalTo(todayInFrench));
        assertThat(googleCalendarViewPage.getBodyBackgroundColor(), endsWith(convertHexToRgbColor(backgroundColor)));
    }

    private String createGoogleCalendarWikiWithParams(String... params) {
        String joinedParams = Joiner.on('|').join(params);
        return "{google-calendar:urls=" + joinedParams + "}";
    }

    private String getFormattedCurrentMonthDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM uuuu");
        return LocalDate.now().format(formatter);
    }

    private String convertHexToRgbColor(String hexColor) {
        Color color = Color.decode(hexColor);
        return "rgb(" + String.join(", ",
                String.valueOf(color.getRed()),
                String.valueOf(color.getGreen()),
                String.valueOf(color.getBlue())) +
                ")";
    }
}