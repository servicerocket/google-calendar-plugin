package it.net.customware.confluence.plugin.google.calendar;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rest.api.ConfluenceRestSession;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.GroupFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.NoOpPage;
import it.net.customware.confluence.plugin.google.calendar.pageobjects.GoogleCalendarViewPage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;

import static com.atlassian.confluence.test.rpc.api.permissions.GlobalPermission.PERSONAL_SPACE;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.*;
import static com.atlassian.confluence.test.stateless.fixtures.GroupFixture.groupFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;

@Ignore
@RunWith(ConfluenceStatelessTestRunner.class)
public abstract class AbstractGoogleCalendarTestRunner {
    protected static final String MALAYSIA_HOLIDAYS_CALENDAR = "https://calendar.google.com/calendar/embed?src=en.malaysia%23holiday%40group.v.calendar.google.com&ctz=Asia%2FKuala_Lumpur";
    protected static final String US_HOLIDAYS_CALENDAR = "https://calendar.google.com/calendar/embed?src=en.usa%23holiday%40group.v.calendar.google.com&ctz=Asia%2FKuala_Lumpur";
    @Fixture
    protected static GroupFixture defaultGroup = groupFixture()
            .globalPermission(PERSONAL_SPACE)
            .build();
    @Fixture
    protected static UserFixture defaultUser = userFixture()
            .globalPermission(PERSONAL_SPACE)
            .group(defaultGroup)
            .build();
    @Fixture
    protected static SpaceFixture defaultSpace = spaceFixture()
            .permission(defaultUser, VIEW, PAGE_EDIT, COMMENT, ATTACHMENT_CREATE)
            .build();

    @Inject
    protected static ConfluenceTestedProduct product;
    @Inject
    protected static ConfluenceRestClient restClient;
    @Inject
    protected static WebDriver webDriver;

    protected final ConfluenceRestSession confluenceRestSession;

    public AbstractGoogleCalendarTestRunner() {
        confluenceRestSession = restClient.createSession(defaultUser.get());
    }

    @BeforeClass
    public static void loginOnce() {
        product.login(defaultUser.get(), NoOpPage.class);
    }

    @AfterClass
    public static void tearDown() {
        product.logOutFast();
        defaultSpace.destroy();
    }

    protected Content createPage(String title, String content) {
        return confluenceRestSession.contentService().create(
                Content.builder(ContentType.PAGE)
                        .title(title)
                        .body(content, ContentRepresentation.WIKI)
                        .space(defaultSpace.get())
                        .build());
    }

    protected GoogleCalendarViewPage visitGoogleCalendarViewPage(Content content) {
        return product.visit(GoogleCalendarViewPage.class, content, webDriver);
    }
}
