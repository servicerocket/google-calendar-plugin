/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.google.calendar;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.confluence.util.GeneralUtil.urlDecode;
import static com.atlassian.confluence.util.GeneralUtil.urlEncode;
import static com.atlassian.confluence.util.velocity.VelocityUtils.getRenderedTemplate;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * TODO: Document this class.
 *
 * @author David Peterson
 */

/*
 * <iframesrc=
 * "http://www.google.com/calendar/hosted/randombits.org/embed?title=MyTitle&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=david%40randombits.org&amp;color=%23BE6D00&amp;ctz=Australia%2FBrisbane"
 * style=" border-width:0 " width="800" height="600" frameborder="0"
 * scrolling="no"></iframe>
 */
public class GoogleCalendarMacro extends BaseMacro implements Macro {

    public enum WeekDay {
        SUNDAY(1, "su"),
        MONDAY(2, "mo"),
        TUESDAY(3, "tu"),
        WEDNESDAY(4, "we"),
        THURSDAY(5, "th"),
        FRIDAY(6, "fr"),
        SATURDAY(7, "sa");

        public final int value;
        private final String prefix;

        private WeekDay( int value, String prefix ) {
            this.value = value;
            this.prefix = prefix;
        }

        public boolean matches( String name ) {
            return name != null && name.toLowerCase().startsWith( prefix );
        }

        /**
         * Finds the matching WeekDay for the provided name. If none matches,
         * {@link #SUNDAY} is returned.
         *
         * @param name
         *            the name.
         * @return The matching value.
         */
        public static WeekDay find( String name ) {
            for ( WeekDay i : values() ) {
                if ( i.matches( name ) )
                    return i;
            }
            return SUNDAY;
        }
    }

    private enum Controls {
        ALL( true, true, true, true, true, true, true ),
        // Just navigation
        NAVIGATION(false, true, true, false, false, false, false),
        // None
        NONE(false, false, false, false, false, false,
                false);

        private boolean title;
        private boolean navigation;
        private boolean date;
        private boolean print;
        private boolean tabs;
        private boolean calendars;
        private boolean tz;

        private Controls( boolean title, boolean navigation, boolean date, boolean print, boolean tabs,
                          boolean calendars, boolean tz ) {
            this.title = title;
            this.navigation = navigation;
            this.date = date;
            this.print = print;
            this.tabs = tabs;
            this.calendars = calendars;
            this.tz = tz;
        }

        public String getUrlParams() {
            StringBuilder out = new StringBuilder();
            append( "showTitle", title, out );
            append( "showNav", navigation, out );
            append( "showDate", date, out );
            append( "showPrint", print, out );
            append( "showTabs", tabs, out );
            append( "showCalendars", calendars, out );
            append( "showTz", tz, out );
            return out.toString();
        }

        private void append( String name, boolean value, StringBuilder out ) {
            if ( !value )
                out.append( name ).append( "=0" ).append( ENCODED_AMPERSAND );
        }

    }

    // private static final String EVENTS_PER_DAY_PARAM = "eventsPerDay";
    private static final String BGCOLOR_PARAM = "bgcolor";
    private static final String CONTROLS_PARAM = "controls";
    private static final String FIRST_DAY_PARAM = "firstDay";
    private static final String HEIGHT_PARAM = "height";
    private static final String LANGUAGE_PARAM = "language";
    private static final String MODE_PARAM = "mode";
    private static final String SRC_PARAM = "src";
    private static final String TIMEZONE_PARAM = "timezone";
    private static final String TITLE_PARAM = "title";
    private static final String URLS_PARAM = "urls";
    private static final String WIDTH_PARAM = "width";

    // http://www.google.com/calendar/feeds/rAnd0ma1pHAnuM@group.calendar.google.com/private-XXXXXXXXXXXX/basic
    // http://www.google.com/calendar/ical/rAnd0ma1pHAnuM@group.calendar.google.com/private-XXXXXXXXXXXX/basic.ics
    // http://www.google.com/calendar/embed?src=rAnd0ma1pHAnuM%40group.calendar.google.com&pvttk=XXXXXXXXXXXXXX
    // original regular expression
    private static final Pattern CALS_PATTERN = Pattern
            .compile( ".*/calendar/(?:feeds/|ical/|embed\\?src=)([^/\\&]+)(?:(?:/private-|&pvttk=)([^/]+))?.*" );

    // regular expression for HTML link
    private static final Pattern CALS_HTML_PATTERN = Pattern
            .compile( ".*/calendar/(?:feeds/|ical/|embed\\?src=)(?:(.*)(?:&ctz=)(?:&pvttk=)?)([^/]+)?.*" );

    // regular expression to validate the link type
    private static final Pattern CALS_CHECKER_PATTERN = Pattern
            .compile( ".*/calendar/(feeds/|ical/|embed\\?src=).*" );

    static final int CALENDAR_HEIGHT_OFFSET = 20;
    static final String BGCOLOR_URL_PARAM = "bgcolor=";
    static final String COLOR_PARAM = "color";
    static final String COLOR_URL_PARAM = "color=";
    static final String COLORS_PARAM = "colors";
    static final String COMMA_SEPARATOR_WITH_SPACE = ",\\s?";
    static final String DEFAULT_FIRST_DAY = "SUNDAY";
    static final String DEFAULT_HEIGHT = "610";
    static final String DEFAULT_MODE = "MONTH";
    static final String DEFAULT_WIDTH = "100%";
    static final String ENCODED_AMPERSAND = "&";
    static final String GOOGLE_CALENDAR_URL_PREFIX = "https://www.google.com/calendar/embed?";
    static final String HEIGHT_URL_PARAM = "height=";
    static final String LANGUAGE_URL_PARAM = "hl=";
    static final String NEWLINE_SEPARATOR = "(\\r?\\n)";
    static final String PVTTK_URL_PARAM = "pvttk=";
    static final String SRC_URL_PARAM = "src=";
    static final String TEMPLATE_LOCATION = "/net/customware/confluence/plugin/google/calendar/calendar-template.vm";
    static final String TIMEZONE_URL_PARAM = "ctz=";
    static final String TITLE_URL_PARAM = "title=";
    static final String WEEKSTART_URL_PARAM = "wkst=";

    @Override
    public String execute(Map parameters, String bodyContent, RenderContext renderContext) throws MacroException {
        try {
            return execute(parameters, bodyContent, new DefaultConversionContext(renderContext));
        } catch (MacroExecutionException e) {
            throw new MacroException(e);
        }
    }

    @Override
    @RequiresFormat(Format.Storage)
    public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException {
        HashMap<String, Object> contextMap = new HashMap<>();
        contextMap.put(SRC_PARAM, getIframeSrc(parameters, bodyContent));
        contextMap.put(WIDTH_PARAM, getOrDefault(parameters, WIDTH_PARAM, DEFAULT_WIDTH));
        contextMap.put(HEIGHT_PARAM, getHeight(parameters));

        return getRenderedTemplate(TEMPLATE_LOCATION, contextMap);
    }

    protected String getIframeSrc(Map<String, String> parameters, String bodyContent) throws MacroExecutionException {
        String colorStr = getOrDefault(parameters, COLOR_PARAM, getOrDefault(parameters, COLORS_PARAM, EMPTY));
        String[] colors = isNotBlank(colorStr) ? colorStr.split(COMMA_SEPARATOR_WITH_SPACE) : null;

        List<GoogleCalendarObject> calendars;
        if (isBlank(parameters.get(URLS_PARAM))) {
            calendars = getCalendarObjList(bodyContent.trim().split(NEWLINE_SEPARATOR), colors);
        } else {
            calendars = getCalendarObjList(parameters.get(URLS_PARAM).trim().split(COMMA_SEPARATOR_WITH_SPACE), colors);
        }

        WeekDay firstDay = WeekDay.find(getOrDefault(parameters, FIRST_DAY_PARAM, DEFAULT_FIRST_DAY));
        int weekStart = firstDay.value;

        Controls controlObjs = Controls.valueOf(getOrDefault(parameters, CONTROLS_PARAM, Controls.NAVIGATION.toString()).toUpperCase());
        String controls = controlObjs.getUrlParams();

        //Form iframe URL
        StringBuilder out = new StringBuilder();
        out.append(GOOGLE_CALENDAR_URL_PREFIX + "mode=")
                .append(getOrDefault(parameters, MODE_PARAM, DEFAULT_MODE).toUpperCase())
                .append(ENCODED_AMPERSAND);
        if (weekStart > WeekDay.SUNDAY.value) {
            out.append(WEEKSTART_URL_PARAM).append(weekStart)
                    .append(ENCODED_AMPERSAND);
        }
        if (isNotBlank(parameters.get(BGCOLOR_PARAM))) {
            out.append(BGCOLOR_URL_PARAM).append(urlEncode(parameters.get(BGCOLOR_PARAM)))
                    .append(ENCODED_AMPERSAND);
        }
        if (isNotBlank(parameters.get(TITLE_PARAM))) {
            out.append(TITLE_URL_PARAM).append(urlEncode(parameters.get(TITLE_PARAM)))
                    .append(ENCODED_AMPERSAND);
        }
        if (isNotBlank(parameters.get(LANGUAGE_PARAM))) {
            out.append(LANGUAGE_URL_PARAM).append(urlEncode(parameters.get(LANGUAGE_PARAM)))
                    .append(ENCODED_AMPERSAND);
        }
        for (GoogleCalendarObject obj : calendars) {
            out.append(obj.toString());
        }
        if (isNotBlank(parameters.get(TIMEZONE_PARAM))) {
            out.append(TIMEZONE_URL_PARAM).append(urlEncode(parameters.get(TIMEZONE_PARAM)))
                    .append(ENCODED_AMPERSAND);
        }
        if (isNotBlank(controls)) {
            out.append(controls);
        }
        out.append(HEIGHT_URL_PARAM).append(urlEncode(String.valueOf(getHeight(parameters) - CALENDAR_HEIGHT_OFFSET)));

        return out.toString();
    }

    protected int getHeight(Map<String, String> parameters) throws MacroExecutionException {
        try {
            return Integer.valueOf(getOrDefault(parameters, HEIGHT_PARAM, DEFAULT_HEIGHT));
        } catch (NumberFormatException nfe) {
            throw new MacroExecutionException("Invalid height value.", nfe);
        }
    }

    private List<String[]> getCalendars(String[] lines) throws MacroExecutionException {
        List<String[]> cals = new java.util.ArrayList<String[]>( lines.length );

        for ( int i = 0; i < lines.length; i++ ) {
            // Skip comments...
            if ( lines[i].startsWith( "//" ) )
                continue;
            // check link type
            Matcher checkerMatcher = CALS_CHECKER_PATTERN.matcher( lines[i] );
            String linkType = "";
            if ( checkerMatcher.matches() )
                linkType = checkerMatcher.group( 1 );

            Matcher matcher = null;

            if ( linkType.startsWith( "embed" ) )
                matcher = CALS_HTML_PATTERN.matcher( lines[i] );
            else
                matcher = CALS_PATTERN.matcher( lines[i] );

            if ( !matcher.matches() )
                throw new MacroExecutionException( "Invalid Google Calendar URL: " + lines[i] );

            String[] cal = new String[2];
            cal[0] = matcher.group( 1 );
            if ( cal[0].indexOf( '%' ) > -1 )
                cal[0] = urlDecode( cal[0] );

            if ( matcher.groupCount() == 2 )
                cal[1] = matcher.group( 2 );

            cals.add( cal );
        }

        return cals;
    }

    protected List<GoogleCalendarObject> getCalendarObjList(String[] urls, String[] colors) throws MacroExecutionException {
        List<String[]> calendarList = getCalendars(urls);
        List<GoogleCalendarObject> calendars = new ArrayList<>();
        for (int i = 0; i < calendarList.size(); ++i) {
            GoogleCalendarObject calendarObj = new GoogleCalendarObject();
            String[] calendarStr = calendarList.get(i);
            calendarObj.setUrl(urlEncode(calendarStr[0]));

            // There seems to be a problem with specifying a pvttk for a single
            // public calendar.
            if (calendarStr[1] != null || calendarList.size() > 1) {
                calendarObj.setPvttk(urlEncode(calendarStr[1]));
            }

            if (colors != null && colors.length > i) {
                calendarObj.setColor(urlEncode(colors[i].trim()));
            }
            calendars.add(calendarObj);
        }
        return calendars;
    }

    /**
     * This method created to ease the process of getting parameters else return default value.
     * Remove this method and use getOrDefault from Java 8 Map in future.
     * @param parameters Map containing parameters from macro
     * @param paramName name of the parameter to be retrieved
     * @param defaultValue the default value to return if no value found
     * @return The param in String or the default value if param is null.
     */
    private String getOrDefault(Map<String, String> parameters, String paramName, String defaultValue) {
        return isNotBlank(parameters.get(paramName)) ? parameters.get(paramName) : defaultValue;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    @Override
    public boolean hasBody() {
        return true;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }
}
