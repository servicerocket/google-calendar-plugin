package net.customware.confluence.plugin.google.calendar;

import org.apache.commons.lang3.StringUtils;

import static net.customware.confluence.plugin.google.calendar.GoogleCalendarMacro.*;

/**
 * @author khailon
 * @since 4.0.0.20160419
 */
public class GoogleCalendarObject {

    private String url;
    private String color;
    private String pvttk;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPvttk() {
        return pvttk;
    }

    public void setPvttk(String pvttk) {
        this.pvttk = pvttk;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append(SRC_URL_PARAM).append(url).append(ENCODED_AMPERSAND);

        if (StringUtils.isNotBlank(color)) {
            out.append(COLOR_URL_PARAM).append(color).append(ENCODED_AMPERSAND);
        }

        if (StringUtils.isNotBlank(pvttk)) {
            out.append(PVTTK_URL_PARAM).append(pvttk).append(ENCODED_AMPERSAND);
        }
        return out.toString();
    }
}
