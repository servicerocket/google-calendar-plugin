package net.customware.confluence.plugin.google.calendar;

import com.atlassian.confluence.macro.xhtml.PlainTextMacroMigration;

public class GoogleCalendarMacroMigrator extends PlainTextMacroMigration{
    public GoogleCalendarMacroMigrator () {
        super();
    }
}
