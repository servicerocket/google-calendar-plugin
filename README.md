# Google Calendar
This is an add-on that was developed by ServiceRocket but was discontinued and now made open-source.

## What is Google Calendar?
Google Calendar lets you view a simple version of a calendar from external websites.
This add-on allows you to do just that from within Confluence. It also makes it easy to have multiple calendars aggregated into a single display.

## How to install
- Download the plugin from the Atlassian Marketplace.
- Go to the Manage Add-ons section:
	- Confluence 5.x: Cog Icon  > Add-ons
	- Confluence 4.x: Browse > Confluence Admin > Add-ons > Manage Add-ons
- Click on Upload Add-on.
- Choose the plugin file (.obr or .jar) from the directory where you downloaded the plugin to.
- Click Upload.
- Ensure the plugin is installed successfully.

## Macro reference
|Name|Default Value|Description|
|---|---|---|
|mode|month|The default calendar mode ("month", "week", or "agenda").|
|controls|navigation|The controls to display ("all", "navigation" or "none").|
|title| |The title to display if displaying "all" controls. Defaults to the first calendar's name.|
|firstDay| |The day of the week to start on ("Sunday", "Monday", etc). May be any day of the week.|
|colors| |A comma-separated list of colours for each of the calendars being displayed. This must be one of the following colours: #A32929, #B1365F, #7A367A, #5229A3, #29527A, #2952A3, #1B887A, #28754E, #0D7813, #528800, #88880E, #AB8B00, #BE6D00, #B1440E, #865A5A, #705770, #4E5D6C, #5A6986, #4A716C, #6E6E41, #8D6F47, #853104, #691426, #5C1158, #23164E, #182C57, #060D5E, #125A12, #2F6213, #2F6309, #5F6B02, #8C500B, #8C500B, #754916, #6B3304, #5B123B, #42104A, #113F47, #333333, #0F4B38, #856508, #711616|
|bgColor| |The background colour ("#AABBCC" or "red", "blue", etc.).|
|width|100%|The width of the calendar (either pixels - "500"- or percentage - "80%").|
|height|610|The height of the calendar in pixels (e.g. "350").|

## Notes
Calendar URLs can be found in the 'Calendar Details' section of a given Google Calendar. You can use any one of the XML, ICAL, or HTML links for a calendar. You can also use a single private link, however be aware that anyone who can view the page will be able to view the URL, so your private calendar will be private no longer.